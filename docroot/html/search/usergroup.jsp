<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects/>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	RowChecker rowChecker = new RowChecker(renderResponse);
	//rowChecker.setRowIds("id");
	//rowChecker.setAllRowIds("categoryAllRowIds");
%>

<!-- In order to display data in Search Container we will use liferay default Search Container -->
<liferay-ui:search-container searchContainer="${userGroupSearchContainer}" rowChecker="<%= rowChecker %>">
	<!-- In order to get the total results and also to display no. of data -->
	<liferay-ui:search-container-results results="<%=searchContainer.getResults()%>" total="<%=searchContainer.getTotal()%>">
		<!-- Display Row with Name and Description as columns -->
		<liferay-ui:search-container-row className="com.liferay.portal.model.UserGroup" keyProperty="userGroupId" modelVar="userGroup">
			<liferay-ui:search-container-column-text name="Name" property="name" />
			<liferay-ui:search-container-column-text name="Description" property="description" />
		</liferay-ui:search-container-row>
		<!-- Iterating the Results -->
		<liferay-ui:search-iterator >
			<liferay-portlet:param name="mvcPath" value="/html/search/usergroup.jsp"/>
		</liferay-ui:search-iterator>
	</liferay-ui:search-container-results>
</liferay-ui:search-container>