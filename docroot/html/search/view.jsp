<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<!-- Making the Render URL and we are passing TAB parameter -->

<portlet:renderURL var="portletTabURL">
	<portlet:param name="tabs" value="${tabs}"/>
</portlet:renderURL>

<!-- Using default Liferay UI Tab in order to show the Tab View -->
<liferay-ui:tabs names="UserGroup,User" param="tabs" refresh="<%=true%>" url="<%=portletTabURL%>">

	<!-- We are going to display two tabs so there will be two Sections in the Tab -->
	<liferay-ui:section>
		<c:if test="${tabs eq 'UserGroup'}">
			<%@include file="/html/search/usergroup.jsp"%>
		</c:if>
	</liferay-ui:section>
	<liferay-ui:section>
		<c:if test="${tabs eq 'User'}">
			<%@include file="/html/search/user.jsp"%>
		</c:if>
	</liferay-ui:section>
</liferay-ui:tabs>
