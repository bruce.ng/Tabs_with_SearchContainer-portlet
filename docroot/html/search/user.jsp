<%@page import="com.liferay.portal.model.UserConstants"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.model.OrganizationConstants"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.model.Organization"%>
<%@page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
<%@page import="com.liferay.portal.kernel.events.Action"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects/>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	RowChecker rowChecker = new RowChecker(renderResponse);
	rowChecker.setRowIds("idUser");
	rowChecker.setAllRowIds("userAllRowIds");
	
	String toolbarItem = ParamUtil.getString(request, "toolbarItem", "browse");
	
	String viewUsersRedirect = ParamUtil.getString(request, "viewUsersRedirect");
	String backURL = ParamUtil.getString(request, "backURL", viewUsersRedirect);
	
	String usersListView = ParamUtil.get(request, "usersListView", UserConstants.LIST_VIEW_TREE);
	
	PortletURL portletURL = renderResponse.createRenderURL();

	portletURL.setParameter("struts_action", "/users_admin/view_users");
	portletURL.setParameter("usersListView", usersListView);

	if (Validator.isNotNull(viewUsersRedirect)) {
		portletURL.setParameter("viewUsersRedirect", viewUsersRedirect);
	}

	String portletURLString = portletURL.toString();
	
%>

<portlet:actionURL var="addEmployeeURL" name="deleteUsers">
</portlet:actionURL>

<aui:form  method="post" action="<%= addEmployeeURL.toString() %>" name="fm">

<!-- In order to display data in Pagination form we will use liferay default Search Container -->
<liferay-ui:search-container searchContainer="${userSearchContainer}" rowChecker="<%= rowChecker %>" var="userSearchContainer">

	<aui:input name="deleteOrganizationIds" type="hidden" />

	<!-- In order to get the total results and also to display no. of data -->
	<liferay-ui:search-container-results results="<%=userSearchContainer.getResults()%>"
		total="<%=userSearchContainer.getTotal()%>" >
		<!-- Display Row with First Name,Last Name, ScreenName and Description as columns -->
		<liferay-ui:search-container-row className="com.liferay.portal.model.User" keyProperty="userId" escapedModel="<%= true %>" modelVar="_user">
			<liferay-ui:search-container-column-text name="First Name" property="firstName" />
			<liferay-ui:search-container-column-text name="Last Name" property="lastName" />
			<liferay-ui:search-container-column-text name="Screen Name" property="screenName" />
			<liferay-ui:search-container-column-text name="Job Title" property="jobTitle" />
		</liferay-ui:search-container-row>
		
		<!-- results là của search-container-reulsts -->
		<c:if test="<%= !results.isEmpty() %>">
			<div class="separator"><!-- --></div>
			<aui:button cssClass="delete-organizations" disabled="<%= true %>" name="delete" type="submit" value="delete" />
		</c:if>
		
		<!-- Iterating the Results -->
		<liferay-ui:search-iterator>
			<liferay-portlet:param name="mvcPath" value="/html/search/user.jsp"/>
			
		</liferay-ui:search-iterator>
	</liferay-ui:search-container-results>
</liferay-ui:search-container>

</aui:form>
<aui:script>
	Liferay.Util.toggleSearchContainerButton('#<portlet:namespace />delete',
											'#<portlet:namespace /><%= searchContainerReference.getId("userSearchContainer") %>SearchContainer',
											document.<portlet:namespace />fm,
											'<portlet:namespace />allRowIds');
	
	function <portlet:namespace />deleteOrganization(organizationId) {
		<portlet:namespace />doDeleteOrganization('<%= Organization.class.getName() %>', organizationId);
	}

	function <portlet:namespace />doDeleteOrganization(className, id) {
		var ids = id;

		var status = <%= WorkflowConstants.STATUS_INACTIVE %>;

		<portlet:namespace />getUsersCount(
			className, ids, status,
			function(event, id, obj) {
				var responseData = this.get('responseData');
				var count = parseInt(responseData);

				if (count > 0) {
					status = <%= WorkflowConstants.STATUS_APPROVED %>

					<portlet:namespace />getUsersCount(
						className, ids, status,
						function(event, id, obj) {
							responseData = this.get('responseData')
							count = parseInt(responseData);

							if (count > 0) {
								if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-you-want-to-delete-this") %>')) {
									<portlet:namespace />doDeleteOrganizations(ids);
								}
							}
							else {
								var message = null;

								if (id && (id.toString().split(",").length > 1)) {
									message = '<%= UnicodeLanguageUtil.get(pageContext, "one-or-more-organizations-are-associated-with-deactivated-users.-do-you-want-to-proceed-with-deleting-the-selected-organizations-by-automatically-unassociating-the-deactivated-users") %>';
								}
								else {
									message = '<%= UnicodeLanguageUtil.get(pageContext, "the-selected-organization-is-associated-with-deactivated-users.-do-you-want-to-proceed-with-deleting-the-selected-organization-by-automatically-unassociating-the-deactivated-users") %>';
								}

								if (confirm(message)) {
									<portlet:namespace />doDeleteOrganizations(ids);
								}
							}
						}
					);
				}
				else {
					if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-you-want-to-delete-this") %>')) {
						<portlet:namespace />doDeleteOrganizations(ids);
					}
				}
			}
		);
	}

	function <portlet:namespace />doDeleteOrganizations(organizationIds) {
		document.<portlet:namespace />fm.method = "post";
		document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "<%= Constants.DELETE %>";
		document.<portlet:namespace />fm.<portlet:namespace />redirect.value = document.<portlet:namespace />fm.<portlet:namespace />organizationsRedirect.value;
		document.<portlet:namespace />fm.<portlet:namespace />deleteOrganizationIds.value = organizationIds;

		submitForm(document.<portlet:namespace />fm, "<portlet:actionURL><portlet:param name="struts_action" value="/users_admin/edit_organization" /></portlet:actionURL>");
	}

	function <portlet:namespace />search() {
		document.<portlet:namespace />fm.method = "post";
		document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "";

		submitForm(document.<portlet:namespace />fm, '<%= portletURLString %>');
	}

	function <portlet:namespace />showUsers(status) {

		<%
		PortletURL showUsersURL = renderResponse.createRenderURL();

		showUsersURL.setParameter("struts_action", "/users_admin/view_users");
		showUsersURL.setParameter("usersListView", usersListView);

		long organizationId = ParamUtil.getLong(request, "organizationId", OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);

		if (organizationId != OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID) {
			showUsersURL.setParameter("organizationId", String.valueOf(organizationId));
		}

		if (Validator.isNotNull(viewUsersRedirect)) {
			showUsersURL.setParameter("viewUsersRedirect", viewUsersRedirect);
		}
		%>

		location.href = Liferay.Util.addParams('<portlet:namespace />status=' + status.value, '<%= showUsersURL.toString() %>');
	}

	Liferay.provide(
		window,
		'<portlet:namespace />deleteOrganizations',
		function() {
			<portlet:namespace />doDeleteOrganization(
				'<%= Organization.class.getName() %>',
				Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds')
			);
		},
		['liferay-util-list-fields']
	);

	Liferay.provide(
		window,
		'<portlet:namespace />deleteUsers',
		function(cmd) {
			if (((cmd == "<%= Constants.DEACTIVATE %>") && confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-you-want-to-deactivate-the-selected-users") %>')) || ((cmd == "<%= Constants.DELETE %>") && confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-you-want-to-permanently-delete-the-selected-users") %>')) || (cmd == "<%= Constants.RESTORE %>")) {
				document.<portlet:namespace />fm.method = "post";
				document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = cmd;
				document.<portlet:namespace />fm.<portlet:namespace />redirect.value = document.<portlet:namespace />fm.<portlet:namespace />usersRedirect.value;
				document.<portlet:namespace />fm.<portlet:namespace />deleteUserIds.value = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');

				submitForm(document.<portlet:namespace />fm, "<portlet:actionURL><portlet:param name="struts_action" value="/users_admin/edit_user" /></portlet:actionURL>");
			}
		},
		['liferay-util-list-fields']
	);

	Liferay.provide(
		window,
		'<portlet:namespace />getUsersCount',
		function(className, ids, status, callback) {
			var A = AUI();

			A.io.request(
				'<%= themeDisplay.getPathMain() %>/users_admin/get_users_count',
				{
					data: {
						className: className,
						ids: ids,
						status: status
					},
					on: {
						success: callback
					}
				}
			);
		},
		['aui-io']
	);
</aui:script>