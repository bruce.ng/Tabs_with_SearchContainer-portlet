<%@page import="vn.vndir.business.Constants"%>
<%@ include file="/html/portlet/init.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	String toolbarItem = ParamUtil.getString(request, "toolbarItem", "ChiNhanh");
	
%>

<aui:nav>
	<aui:nav-item dropdown="<%=true%>" iconCssClass="icon-plus" label="add" selected='<%= toolbarItem.equals("add") %>'>
		<!-- -------------Chi Nhanh ------  -->
		<portlet:renderURL var="viewChiNhanhsURL">
			<portlet:param name="tabs" value="ChiNhanh" />
		</portlet:renderURL>
		
		<portlet:renderURL var="addChiNhanhURL">
			<portlet:param name="jspPage" value="/html/portlet/agent/chi_nhanh_edit.jsp"/>
			<portlet:param name="backURL" value="<%=viewChiNhanhsURL%>" />
			<portlet:param name="redirect" value="<%=viewChiNhanhsURL%>" />
			<portlet:param name="<%=Constants.ACTION_TAKEN%>" value="<%=Constants.STATUS_CREATE%>"/>
		</portlet:renderURL>
	
		<aui:nav-item href="<%=addChiNhanhURL%>" iconCssClass="icon-globe" label="Chi Nhánh" />
		
		
		<aui:nav-item cssClass="divider" />
		
		<!-- -------------Don vi hanh chinh------  -->
		<portlet:renderURL var="viewDonViHanhChinhsURL">
			<portlet:param name="tabs" value="DonViHanhChinh" />
		</portlet:renderURL>
		
		<portlet:renderURL var="addDonViHanhChinhURL">
			<portlet:param name="jspPage" value="/html/portlet/agent/donvi_hanhchinh_edit.jsp" />
			<portlet:param name="backURL" value="<%=viewDonViHanhChinhsURL%>" />
			<portlet:param name="redirect" value="<%=viewDonViHanhChinhsURL%>" />
			<portlet:param name="<%=Constants.ACTION_TAKEN%>" value="<%=Constants.STATUS_CREATE%>"/>
		</portlet:renderURL>
		
		<aui:nav-item href="<%=addDonViHanhChinhURL%>" iconCssClass="icon-user" label="Đơn vị hành chính" />
		
	</aui:nav-item>
</aui:nav>